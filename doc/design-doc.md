# Vundersite Design Doc
A wonderful site for teams

## Overview
Vundersite is a web application that helps teams work together. It is simple and intuitive by design, offering the basic features that a team needs to keep track of their own work and the work of their peers. The initial features will be "scratch my own itch" capabilities implemented as independent services that can combined into an integrated application.

The thinking is that each capability/feature will be stood up as an API-based micro-service.

*Figure 1 - Core Features*

Feature             | Description
------------------- | -----------
VunderView         | A directory listing of everyone within a company, along with meta-data such as which department they fall within, any projects they are supporting, their direct manager(s), their company title, project roles, and contact information.
VunderWiki         | Corporate, Project, and Dev Team level information helpful to new and long-time employees. Use cases include onboarding of new staff, other related newcomer information, and a central location for technical and operational processes.
VunderOutline      | A tool for capturing information and parent/child relationship of the data. Essentially an outlining tool that can be used for a number of use cases, including time-based data (release plan, iteration plan) and product-based organization (functional decomposition).
VunderCal          | A single, shared calendar, as opposed to sending meeting invites to everyone's personal calendars. Ability to filter by multiple criteria, such as corporate department, project, and dev team.
VunderDocs         | A better SharePoint? On par with Google Docs?
VunderChat         | A Slack clone?

## User Interface
So what would something like this look like?

## Use Case Specification

### Context Diagram

### Use Cases

*Figure 2 - Use Cases*

ID   | Name            | Description
---- | --------------- | -----------
01   | Login           | Now is the time for all good men to come to the aid of their party.
02   | Second Use Case | The quick brown fox jumped over the lazy dong.
03   | Third Use Case  | Well done beats well said.
04   | Fourth Use Case | Those who lie with dogs wake up with flees.
05   | Fifth Use Case  | A stitch in time saves nine.
06   | Logout          | Many a little makes a mickle.      

